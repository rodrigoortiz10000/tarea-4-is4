<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alquiler extends Model
{
    protected $fillable = [
        'fecha', 'hora_desde', 'hora_hasta', 'devuelto', 'retirado', 'proyector_id', 'aula_id', 'persona_id',
    ];

    // Puedes definir relaciones o métodos adicionales aquí si es necesario
}
