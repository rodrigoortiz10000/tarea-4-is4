<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyector extends Model
{
    protected $table = 'proyectores';

    protected $fillable = [
        'marca', 'modelo', 'fecha_adquisicion', 'numero', 'activo',
    ];

    // Puedes definir relaciones o métodos adicionales aquí si es necesario
}
