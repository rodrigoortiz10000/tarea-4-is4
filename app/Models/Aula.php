<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    protected $fillable = [
        'piso', 'numero', 'activo',
    ];

    // Puedes definir relaciones o métodos adicionales aquí si es necesario
}
