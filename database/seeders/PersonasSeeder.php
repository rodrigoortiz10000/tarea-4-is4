<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Persona;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $personas = [
            [
                'nombre_completo' => 'Juan Pérez',
                'es_profesor' => true,
                'es_alumno' => false,
                'cedula' => '1234567890123456',
            ],
            [
                'nombre_completo' => 'María Rodríguez',
                'es_profesor' => false,
                'es_alumno' => true,
                'cedula' => '9876543210987654',
            ],
            [
                'nombre_completo' => 'Carlos González',
                'es_profesor' => true,
                'es_alumno' => true,
                'cedula' => '1111222233334444',
            ],
            [
                'nombre_completo' => 'Ana López',
                'es_profesor' => false,
                'es_alumno' => true,
                'cedula' => '5555666677778888',
            ],
            [
                'nombre_completo' => 'Pedro Ramírez',
                'es_profesor' => true,
                'es_alumno' => false,
                'cedula' => '9999888877776666',
            ],
        ];

        foreach ($personas as $persona) {
            Persona::create($persona);
        }
    }
}
