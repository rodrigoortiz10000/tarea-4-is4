<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\ValoracionesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/categorias', [CategoriasController::class, 'index']);
Route::post('/categorias', [CategoriasController::class, 'store']);
Route::put('/categorias/{id}', [CategoriasController::class, 'update']);
Route::delete('/categorias/{id}', [CategoriasController::class, 'destroy']);
Route::get('/categorias/{id}', [CategoriasController::class, 'show']);


Route::get('/productos', [ProductosController::class, 'index']);
Route::post('/categorias/{categoria}/productos', [ProductosController::class, 'store']);
Route::put('/productos/{id}', [ProductosController::class, 'update']);
Route::delete('/productos/{id}', [ProductosController::class, 'destroy']);
//Route::get('/categorias/{id}/productos/{id}', [ProductosController::class, 'show']);
Route::get('/productos/{id}', [ProductosController::class, 'show']);

Route::post('/productos/{producto}/valoraciones', [ValoracionesController::class, 'store']);
Route::get('/productos/{id}', [ProductosController::class, 'show']);
Route::put('/valoraciones/{id}', [ValoracionesController::class, 'update']);