<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('alquiler', function (Blueprint $table) {
            $table->bigIncrements('id'); // Campo de clave primaria autoincremental (serial)
            $table->date('fecha');
            $table->time('hora_desde');
            $table->time('hora_hasta');
            $table->boolean('devuelto')->default(false);
            $table->boolean('retirado')->default(true);

            // Claves foráneas
            $table->unsignedBigInteger('proyector_id');
            $table->unsignedBigInteger('aula_id');
            $table->unsignedBigInteger('persona_id');

            // Timestamps para created_at y updated_at
            $table->timestamps();

            // Relaciones con las tablas proyectores, aulas y personas
            $table->foreign('proyector_id')->references('id')->on('proyectores');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('alquiler');
    }
};
