<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Proyector;
class ProyectoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $proyectores = [
            [
                'marca' => 'Epson',
                'modelo' => 'PowerLite XGA',
                'fecha_adquisicion' => now(),
                'numero' => 'P001',
                'activo' => true,
            ],
            [
                'marca' => 'BenQ',
                'modelo' => 'MX528',
                'fecha_adquisicion' => now(),
                'numero' => 'P002',
                'activo' => true,
            ],
            [
                'marca' => 'Sony',
                'modelo' => 'VPL-DX240',
                'fecha_adquisicion' => now(),
                'numero' => 'P003',
                'activo' => true,
            ],
            [
                'marca' => 'Acer',
                'modelo' => 'H6517ST',
                'fecha_adquisicion' => now(),
                'numero' => 'P004',
                'activo' => true,
            ],
            [
                'marca' => 'Optoma',
                'modelo' => 'HD27e',
                'fecha_adquisicion' => now(),
                'numero' => 'P005',
                'activo' => true,
            ],
        ];

        foreach ($proyectores as $proyector) {
            Proyector::create($proyector);
        }

    }
}
