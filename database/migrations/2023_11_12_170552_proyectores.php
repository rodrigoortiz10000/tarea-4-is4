<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('proyectores', function (Blueprint $table) {
            $table->bigIncrements('id'); // Campo de la clave primaria
            $table->string('marca', 40);
            $table->string('modelo', 20);
            $table->date('fecha_adquisicion');
            $table->string('numero', 6);
            $table->boolean('activo')->default(true); // Valor por defecto activo

            // Timestamps para created_at y updated_at
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('proyectores');
    }
};
