<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = [
        'nombre_completo', 'es_profesor', 'es_alumno', 'cedula',
    ];

    // Puedes definir relaciones o métodos adicionales aquí si es necesario
}
